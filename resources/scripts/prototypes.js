import {sortBy, uniq} from 'lodash';

Array.prototype.uniq = function () {
    return uniq(this);
};

Array.prototype.sortBy = function () {
    return sortBy(this);
};

Array.prototype.hasAny = function (other) {
    return this.some(function (value) {
        return other.includes(value);
    });
};

String.prototype.includesCase = function (search, position = 0) {
    const lower = (search || '').toLowerCase();
    return this.toLowerCase().includes(lower, position);
};